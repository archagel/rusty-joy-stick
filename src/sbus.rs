use arraydeque::{ArrayDeque, Wrapping};
use std::cmp::{max, min};

// The flag by should start with 4 0s
const SBUS_HEADER_BYTE: u8 = 0x0F;
const SBUS_FOOTER_BYTE: u8 = 0b00000000;
const CH17: u8 = 0x01;
const CH18: u8 = 0x02;
const LOST_FRAME: u8 = 0x04;
const FAILSAFE: u8 = 0x08;

const SBUS_PACKET_SIZE: usize = 25;

#[derive(Debug, Ord, PartialOrd, Eq, PartialEq, Copy, Clone)]
pub struct SBusPacket {
    pub channels: [u16; 16],
    d1: bool,
    d2: bool,
    pub failsafe: bool,
    pub frame_lost: bool,
}

pub struct SBusPacketParser {
    buffer: ArrayDeque<[u8; (SBUS_PACKET_SIZE * 2) as usize], Wrapping>,
}

impl SBusPacketParser {
    pub fn new() -> SBusPacketParser {
        SBusPacketParser {
            buffer: ArrayDeque::new(),
        }
    }

    pub fn push_bytes(&mut self, bytes: &[u8]) {
        bytes.iter().for_each(|b| {
            self.buffer.push_back(*b);
        })
    }

    pub fn try_parse(&mut self) -> Option<SBusPacket> {
        // We can't have a packet if we don't have enough bytes
        if self.buffer.len() < SBUS_PACKET_SIZE {
            return None;
        }

        // If the first byte is not a header byte,
        if *self.buffer.get(0).unwrap() != SBUS_HEADER_BYTE {
            while self.buffer.len() > 0 && *self.buffer.get(0).unwrap() != SBUS_HEADER_BYTE {
                let _ = self.buffer.pop_front().unwrap();
                //println!("Popped byte: {:#?}", popped);
            }

            return None;
        } else if *self.buffer.get(SBUS_PACKET_SIZE - 1).unwrap() == SBUS_FOOTER_BYTE {
            // This seems like a valid packet!
            // Start popping the bytes

            let mut data_bytes: [u16; 23] = [0; 23];
            for i in 0..23 {
                data_bytes[i] = self.buffer.pop_front().unwrap_or(0) as u16;
            }

            let mut channels: [u16; 16] = [0; 16];

            channels[0] = (((data_bytes[1]) | (data_bytes[2] << 8)) as u16 & 0x07FF).into();
            channels[1] = ((((data_bytes[2] >> 3) | (data_bytes[3] << 5)) as u16) & 0x07FF).into();
            channels[2] = ((((data_bytes[3] >> 6) | (data_bytes[4] << 2) | (data_bytes[5] << 10))
                as u16)
                & 0x07FF)
                .into();
            channels[3] = ((((data_bytes[5] >> 1) | (data_bytes[6] << 7)) as u16) & 0x07FF).into();
            channels[4] = ((((data_bytes[6] >> 4) | (data_bytes[7] << 4)) as u16) & 0x07FF).into();
            channels[5] = ((((data_bytes[7] >> 7) | (data_bytes[8] << 1) | (data_bytes[9] << 9))
                as u16)
                & 0x07FF)
                .into();
            channels[6] = ((((data_bytes[9] >> 2) | (data_bytes[10] << 6)) as u16) & 0x07FF).into();
            channels[7] =
                ((((data_bytes[10] >> 5) | (data_bytes[11] << 3)) as u16) & 0x07FF).into();
            channels[8] = ((((data_bytes[12]) | (data_bytes[13] << 8)) as u16) & 0x07FF).into();
            channels[9] =
                ((((data_bytes[13] >> 3) | (data_bytes[14] << 5)) as u16) & 0x07FF).into();
            channels[10] =
                ((((data_bytes[14] >> 6) | (data_bytes[15] << 2) | (data_bytes[16] << 10)) as u16)
                    & 0x07FF)
                    .into();
            channels[11] =
                ((((data_bytes[16] >> 1) | (data_bytes[17] << 7)) as u16) & 0x07FF).into();
            channels[12] =
                ((((data_bytes[17] >> 4) | (data_bytes[18] << 4)) as u16) & 0x07FF).into();
            channels[13] =
                ((((data_bytes[18] >> 7) | (data_bytes[19] << 1) | (data_bytes[20] << 9)) as u16)
                    & 0x07FF)
                    .into();
            channels[14] =
                ((((data_bytes[20] >> 2) | (data_bytes[21] << 6)) as u16) & 0x07FF).into();
            channels[15] =
                ((((data_bytes[21] >> 5) | (data_bytes[22] << 3)) as u16) & 0x07FF).into();

            let flag_byte = self.buffer.pop_front().unwrap_or(0);
            // Pop footer byte
            self.buffer.pop_front();
            return Some(SBusPacket {
                channels,
                d1: is_flag_set(flag_byte, CH17),
                d2: is_flag_set(flag_byte, CH18),
                frame_lost: is_flag_set(flag_byte, LOST_FRAME),
                failsafe: is_flag_set(flag_byte, FAILSAFE),
            });
        } else {
            // We had a header byte, but this doesnt appear to be a valid frame, we are probably out of sync
            // Pop until we find a header again
            loop {
                self.buffer.pop_front();
                if self.buffer.len() <= 0 || *self.buffer.get(0).unwrap() == SBUS_HEADER_BYTE {
                    break;
                }
            }
            return self.try_parse();
        }
    }
}

fn is_flag_set(flag_byte: u8, flag: u8) -> bool {
    flag_byte & flag == flag
}

const OPENTX_MIN: i32 = 1000;
const OPENTX_CENTER: i32 = 1500;
const OPENTX_MAX: i32 = 2000;

fn sbus_raw_to_opentx(sbus_raw: u16) -> u16 {
    ((610127 * sbus_raw as u32 + 895364000) / 1000000) as u16
}

pub fn sbus_to_float_axis(sbus_raw: u16) -> f32 {
    let normalized = min(
        OPENTX_MAX,
        max(OPENTX_MIN, sbus_raw_to_opentx(sbus_raw) as i32),
    ) as f32;

    let proportion = match normalized < OPENTX_CENTER as f32 {
        true => {
            -1.0 * (OPENTX_CENTER as f32 - normalized) / (OPENTX_CENTER as f32 - OPENTX_MIN as f32)
        }
        false => (normalized - OPENTX_CENTER as f32) / (OPENTX_MAX as f32 - OPENTX_CENTER as f32),
    };
    proportion / 2.0 + 0.5
}

pub fn sbus_to_button_press(sbus_raw: u16) -> bool {
    sbus_raw_to_opentx(sbus_raw) as i32 > OPENTX_CENTER
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn find_packet_start() {
        let p1: [u8; 25] = [
            7, 62, 240, 129, 15, 124, 0, 0, 15, 172, 0, 95, 247, 192, 199, 10, 86, 176, 130, 21,
            224, 3, 31, 248, 192,
        ];
        let p2: [u8; 25] = [
            7, 62, 240, 129, 15, 124, 0, 0, 15, 172, 8, 223, 246, 192, 199, 10, 86, 176, 130, 21,
            224, 3, 31, 248, 192,
        ];

        let mut parser = SBusPacketParser::new();
        parser.push_bytes(&p1);
        assert!(parser.try_parse().is_none());
        parser.push_bytes(&p2);
        assert!(parser.try_parse().is_some())
    }
}
