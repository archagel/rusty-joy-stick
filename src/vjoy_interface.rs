use libloading::Library;

#[repr(C)]
pub struct JoystickState {
    pub b_device: u8,
    pub throttle: i32,
    pub rudder: i32,
    pub aileron: i32,
    pub axis_x: i32,
    pub axis_y: i32,
    pub axis_z: i32,
    pub axis_x_rotation: i32,
    pub axis_y_rotation: i32,
    pub axis_z_rotation: i32,
    pub slider: i32,
    pub dial: i32,
    pub wheel: i32,
    pub axis_v_x: i32,
    pub axis_v_y: i32,
    pub axis_v_z: i32,
    pub axis_v_br_x: i32,
    pub axis_v_br_y: i32,
    pub axis_v_br_z: i32,
    pub buttons: u32,
    pub b_hats: u32,
    pub b_hats_ex1: u32,
    pub b_hats_ex2: u32,
    pub b_hats_ex3: u32,
    pub buttons_ex1: u32,
    pub buttons_ex2: u32,
    pub buttons_ex3: u32,
}

pub struct VJoyInterface {
    lib: Library,
}

impl VJoyInterface {
    pub fn new() -> VJoyInterface {
        unsafe {
            let lib = libloading::Library::new("vJoyInterface.dll").unwrap();
            return VJoyInterface { lib };
        }
    }

    pub fn get_joy_sticks(&self) -> Vec<u32> {
        unsafe {
            let func: libloading::Symbol<unsafe extern "C" fn(u32) -> bool> =
                self.lib.get(b"isVJDExists").unwrap();

            let mut joys: Vec<u32> = vec![0; 0];
            for i in 1..16 {
                if func(i) {
                    joys.push(i as u32);
                }
            }

            return joys;
        }
    }

    pub fn acquire(&self, id: u32) -> bool {
        unsafe {
            let func: libloading::Symbol<unsafe extern "C" fn(u32) -> bool> =
                self.lib.get(b"AcquireVJD").unwrap();
            return func(id);
        }
    }

    pub fn update_state(&self, id: u32, state: &JoystickState) -> bool {
        unsafe {
            let func: libloading::Symbol<unsafe extern "C" fn(u32, &JoystickState) -> bool> =
                self.lib.get(b"UpdateVJD").unwrap();
            return func(id, state);
        }
    }

    // pub fn relinquish(&self, id: u32) {
    //     unsafe {
    //         let func: libloading::Symbol<unsafe extern "C" fn(u32) -> bool> =
    //             self.lib.get(b"RelinquishVJD").unwrap();
    //         func(id);
    //     }
    // }
}
