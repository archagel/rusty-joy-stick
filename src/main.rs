mod sbus;
mod serial_reader;
// mod vjoy_debugger;
mod vjoy_interface;
mod vjoy_writer;

use crate::sbus::SBusPacket;
use crate::serial_reader::SerialReader;

use crate::vjoy_writer::VJoyWriter;
use std::io;
use std::io::BufRead;
use std::sync::mpsc::channel;

fn main() {
    let (tx, rx) = channel::<Option<SBusPacket>>();
    let reader = SerialReader::new(tx);
    let writer = VJoyWriter::new(rx, 250);
    writer.start();
    reader.start();
    let mut line = String::new();
    let stdin = io::stdin();
    stdin.lock().read_line(&mut line).unwrap();
}
