use crate::sbus::SBusPacket;
use std::cmp::{max, min};
use std::sync::mpsc::{Receiver, RecvTimeoutError};
use std::thread;
use std::time::{Duration, Instant};

pub struct VJoyDebugger {
    rx: Receiver<Option<SBusPacket>>,
    failsafe_timeout: Duration,
}
impl VJoyDebugger {
    pub fn new(rx: Receiver<Option<SBusPacket>>, failsafe_timeout: u8) -> VJoyDebugger {
        VJoyDebugger {
            rx,
            failsafe_timeout: Duration::from_millis(failsafe_timeout as u64),
        }
    }

    pub fn start(self) {
        thread::spawn(move || {
            let mut time = Instant::now();
            let mut min_delay: u128 = u128::MAX;
            let mut max_delay: u128 = u128::MIN;
            let mut min_throttle: u16 = u16::MAX;
            let mut total_delay: u128 = 0;
            let mut time_to_print: u128 = 0;
            let mut count: u128 = 0;
            let mut failsafe: bool = false;
            let mut lost_frame: bool = false;
            let mut packet: Option<SBusPacket>;
            let mut packets_lost = 0;

            loop {
                match self.rx.recv_timeout(self.failsafe_timeout) {
                    Ok(p) => {
                        packet = p;
                    }
                    Err(err) => {
                        if err == RecvTimeoutError::Disconnected {
                            panic!("Serial sender disconnected!")
                        }
                        packet = None;
                    }
                }
                if packet.is_none() {
                    packets_lost += 1;
                }
                let now = Instant::now();
                let elapsed = now.duration_since(time).as_millis();
                time = now;
                time_to_print += elapsed;

                min_delay = min(min_delay, elapsed);
                max_delay = max(max_delay, elapsed);
                let throttle = match packet {
                    None => 0,
                    Some(p) => p.channels[0],
                };
                min_throttle = min(min_throttle, throttle);
                total_delay += elapsed;
                count += 1;
                failsafe = failsafe || packet.is_none() || packet.unwrap().failsafe;
                lost_frame = lost_frame || (packet.is_some() && packet.unwrap().frame_lost);

                if time_to_print > 1000 {
                    println!(
                        "Avg {:4}ms, Min {:4}ms, Max {:4}ms, Lost: {:3}, Count: {:3}, Frame lost: {:5}, Failsafe: {:5}, Min Throttle: {:4}",

                        total_delay / count,
                        min_delay,
                        max_delay,
                        packets_lost,
                        count,
                        lost_frame,
                        failsafe,
                        min_throttle
                    );
                    time_to_print = 0;
                    packets_lost = 0;
                    min_delay = u128::MAX;
                    max_delay = u128::MIN;
                    min_throttle = u16::MAX;
                    total_delay = 0;
                    count = 0;
                    failsafe = false;
                    lost_frame = false;
                }
            }
        });
    }
}
