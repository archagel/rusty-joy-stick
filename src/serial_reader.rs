use crate::sbus::{SBusPacket, SBusPacketParser};
use serialport::{available_ports, DataBits, Parity, SerialPortInfo, StopBits};
use std::io::Read;
use std::sync::mpsc::Sender;
use std::thread;

pub struct SerialReader {
    port_info: SerialPortInfo,
    tx: Sender<Option<SBusPacket>>,
}
// let (tx, rx) = channel::<SBusPacket>();
impl SerialReader {
    pub fn new(tx: Sender<Option<SBusPacket>>) -> SerialReader {
        let mut ports = available_ports().expect("Unable to enumerate serial ports");
        if ports.is_empty() {
            panic!("No serial ports available");
        }
        let port_info = ports.remove(0);
        SerialReader { port_info, tx }
    }

    pub fn start(self) {
        thread::spawn(move || {
            let mut port = serialport::new(&self.port_info.port_name, 100000)
                .data_bits(DataBits::Eight)
                .parity(Parity::Even)
                .stop_bits(StopBits::Two)
                .open()
                .expect("Failed to open port");

            let mut serial_buf = vec![0; 25];
            let mut parser = SBusPacketParser::new();

            // Start reading loop
            loop {
                let bytes_read = port.read(serial_buf.as_mut_slice());
                if bytes_read.is_err() || bytes_read.unwrap() == 0 {
                    continue;
                }
                parser.push_bytes(serial_buf.as_slice());
                let packet = parser.try_parse();
                self.tx
                    .send(packet)
                    .expect("Serial reader failed to send SBUS packet");
            }
        });
    }
}
