use crate::sbus::{sbus_to_button_press, sbus_to_float_axis, SBusPacket};
use crate::vjoy_interface::{JoystickState, VJoyInterface};
use std::sync::mpsc::{Receiver, RecvTimeoutError};
use std::thread;
use std::time::Duration;

pub struct VJoyWriter {
    rx: Receiver<Option<SBusPacket>>,
    failsafe_timeout: Duration,
}

impl VJoyWriter {
    pub fn new(rx: Receiver<Option<SBusPacket>>, failsafe_timeout: u8) -> VJoyWriter {
        VJoyWriter {
            rx,
            failsafe_timeout: Duration::from_millis(failsafe_timeout as u64),
        }
    }

    pub fn start(self) {
        thread::spawn(move || {
            let interface = VJoyInterface::new();
            let joys = interface.get_joy_sticks();
            let joy = *joys.first().unwrap();
            interface.acquire(joy);
            let mut state: JoystickState = JoystickState {
                b_device: 0,
                throttle: 0,
                rudder: 0,
                aileron: 0,
                axis_x: 0,
                axis_y: 0,
                axis_z: 0,
                axis_x_rotation: 0,
                axis_y_rotation: 0,
                axis_z_rotation: 0,
                slider: 0,
                dial: 0,
                wheel: 0,
                axis_v_x: 0,
                axis_v_y: 0,
                axis_v_z: 0,
                axis_v_br_x: 0,
                axis_v_br_y: 0,
                axis_v_br_z: 0,
                buttons: 0,
                b_hats: 0,
                b_hats_ex1: 0,
                b_hats_ex2: 0,
                b_hats_ex3: 0,
                buttons_ex1: 0,
                buttons_ex2: 0,
                buttons_ex3: 0,
            };
            loop {
                let packet = match self.rx.recv_timeout(self.failsafe_timeout) {
                    Ok(p) => match p {
                        None => {
                            continue;
                        }
                        Some(p) => p,
                    },
                    Err(err) => {
                        if err == RecvTimeoutError::Disconnected {
                            panic!("Serial sender disconnected!")
                        }
                        continue;
                    }
                };
                state.axis_x = sbus_to_vjoy_axis(packet.channels[0]);
                state.axis_y = sbus_to_vjoy_axis(packet.channels[1]);
                state.axis_z = sbus_to_vjoy_axis(packet.channels[2]);
                state.axis_x_rotation = sbus_to_vjoy_axis(packet.channels[3]);
                state.buttons = sbus_to_vjoy_button(packet.channels[4], state.buttons, 0);
                state.buttons = sbus_to_vjoy_button(packet.channels[5], state.buttons, 1);
                interface.update_state(joy, &state);
            }
        });
    }
}

fn sbus_to_vjoy_axis(sbus_raw: u16) -> i32 {
    (1.0 + sbus_to_float_axis(sbus_raw) * 32767.0) as i32
}

fn sbus_to_vjoy_button(sbus_raw: u16, buttons: u32, button: u8) -> u32 {
    let mask = 1 << button;
    match sbus_to_button_press(sbus_raw) {
        true => buttons | mask,
        false => buttons & !mask,
    }
}
