use std::path::{Path, PathBuf};
use std::{env, fs};

fn get_output_path() -> PathBuf {
    //<root or manifest path>/target/<profile>/
    let manifest_dir_string = env::var("CARGO_MANIFEST_DIR").unwrap();
    let build_type = env::var("PROFILE").unwrap();
    let path = Path::new(&manifest_dir_string)
        .join("target")
        .join(build_type);
    return PathBuf::from(path);
}

fn main() {
    let target_dir = get_output_path();
    let src = &env::current_dir()
        .unwrap()
        .join("vendor")
        .join("vJoyInterface.dll");
    let dest = Path::new(&target_dir).join("vJoyInterface.dll");
    fs::copy(src, dest).unwrap();
}
